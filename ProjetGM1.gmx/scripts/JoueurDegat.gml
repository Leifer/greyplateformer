//Dégats sur le joueur
//Si le joueur à 3 points de vie
if (global.pvJ == 3){
   //Perd 1 point de vie
   global.pvJ = 2;
   //Saut
   vspeed = -15;
   //Déclenche l'alarme
   alarm[0] = room_speed/3;
   //Rend invulnérable
   global.invul = 1;
   //Change la couleur du sprite
   image_blend = c_red;
}

//Si le joueur à 2 points de vie
else if (global.pvJ == 2 && global.invul == 0){
   //Perd 1 point de vie
   global.pvJ = 1;
   //Saut
   vspeed = -15;
   //Déclenche l'alarme
   alarm[0] = room_speed/3;
   //Rend invulnérable
   global.invul = 1;
   //Change la couleur du sprite
   image_blend = c_red;
}

//Si le joueur à 1 point de vie
else if (global.pvJ == 1 && global.invul == 0){
   //Perd 1 point de vie
   global.pvJ = 0;
   //Saut
   vspeed = -15;
   //Déclenche l'alarme
   alarm[0] = room_speed/3;
   //Rend invulnérable
   global.invul = 1;
   //Change la couleur du sprite
   image_blend = c_red;
}
