//VARIABLES
//Vitesses
var Ispd = .3;
var Hspd = 5;
var Vspd = 15;
var grav = 1;

//Raccourcis claviers
if obj_interface_system.levelend == 0 {
var Rkey = keyboard_check(vk_right) || keyboard_check(ord('D'));
var Lkey = keyboard_check(vk_left) || keyboard_check(ord('Q'));
var Jkey = keyboard_check(vk_up) || keyboard_check(ord('Z'));
var Fkey = keyboard_check(vk_space);
} else { var Rkey=0; var Lkey=0; var Jkey=0; var Fkey=0;}

//Sprites
var Rspr = spr_joueur_dep_D;
var Lspr = spr_joueur_dep_G;
var Fspr = spr_joueur_stat;

//DEPLACEMENTS
//Déplacement à droite
if (Rkey && place_free(x+Hspd,y)){
   x += Hspd;
   sprite_index = Rspr;
   image_speed = Ispd;
} 

//Déplacement à gauche
else if (Lkey && place_free(x-Hspd,y)){
   x -= Hspd;
   sprite_index = Lspr;
   image_speed = Ispd;
} 

//Sprite si aucun déplacement
else {
   sprite_index = Fspr;
   image_speed = Ispd;
}

//GRAVITE & SAUT
if (place_free(x,y+1)){
   gravity = grav;
} else {
   gravity = 0;
   //Saut
   if (Jkey){
      vspeed = -Vspd;
   }
}

//CAMERA
view_xview[0] += ((x - (view_wview[0]/2) - view_xview)) * .1;
view_yview[0] += ((y - (view_hview[0]/2) - view_yview)) * .1;

//Direction de tir
//Droite
if Rkey { 
global.dirtir = 0 
}
//Gauche
if Lkey { 
global.dirtir = 1 
}


//Système de tir
var xb = x+sprite_width;

if global.dirtir == 0 {
xb = x+sprite_width;
}
if global.dirtir == 1 {
xb = x;
}
if global.balles > 0 && Fkey && alarm[1] <= 0 {
   instance_create(xb,y+(sprite_height/2),obj_balle);
   global.balles -= 1;
   alarm[1] = room_speed/2;
}


