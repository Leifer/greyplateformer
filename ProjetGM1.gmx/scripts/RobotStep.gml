

//Déplacement à droite
if dirERo == 0{
   x += 1
   sprite_index = spr_robot_dep_D;
   image_speed = .3;
}

//Déplacement à gauche
if dirERo == 1{
   x -= 1
   sprite_index = spr_robot_dep_G;
   image_speed = .3;
}

//Changement de direction (vers la droite)
if (place_meeting(x-1,y,obj_bloc_solide) || place_empty(x-(sprite_width/2),y+1)){
   dirERo = 0;
}

//Changement de direction (vers la gauche)
if (place_meeting(x+1,y,obj_bloc_solide) || place_empty(x+(sprite_width/2),y+1)){
   dirERo = 1;
}
