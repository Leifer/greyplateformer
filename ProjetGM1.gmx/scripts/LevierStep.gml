//Variable raccourcis
var Akey = keyboard_check(ord('E'));

//Vérification de contact avec le joueur
if place_meeting(x,y,obj_joueur){
   contact = 1;
} else { contact = 0 }

//Condition pour activer le levier
if contact == 1 && Akey {
   image_yscale = -1;
   etat = 1;
}

//Activation du levier
if etat == 1 {
   with obj_porte_sol {
        if ID == other.ID {
           move = 1;
        }
   }
}
