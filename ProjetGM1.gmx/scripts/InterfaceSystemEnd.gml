//Variables de bases
nbee = instance_number(obj_robot); //Robot
nbme = instance_number(obj_munitions)*5; // Munitions
prec = 0;

//Calcule du score
//Robots détruits
nbek = nbes - nbee;

//Munitions ramassées
nbm = nbms - nbme;

//Balles tirées
nbbt = nbm - global.balles;

//Précision parfaite
if !instance_exists(obj_robot) && nbes == nbbt {
   prec = 100;
}

//Score Global
global.scores = (nbek*10) + nbm + prec + timer;

//Affichage Score Global
draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_text(view_wview/2,view_hview/2,':: SCORE :: ##'
+ 'Ennemis detruits : ' + string(nbek) + 'pts#'
+ 'Munitions Ramassees : ' + string(nbm) + 'pts#'
+ 'Precision Parfaite : ' + string(prec) + 'pts#'
+ 'Temps restant : ' + string(timer) + 'pts##'
+ 'SCORE GLOBAL : ' + string(global.scores)
);
draw_set_valign(fa_top);
draw_set_halign(fa_left);
