//Décompte de timer
if alarm[0] <= 0 && levelend == 0 {
   alarm[0] = room_speed;
   timer --;
}

//Affichage du timer
draw_set_halign(fa_center);
draw_text(view_wview/2,view_vborder, 'TIME :' + string(timer));
draw_set_halign(fa_left);

//Reboot de la room à la fin du timer
if timer <= 0 {
   room_restart();
}
