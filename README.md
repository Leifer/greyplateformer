GreyPlateformer est le nom du projet qui est issu de la série de vidéos tutoriel "Comment Créer un Plateformer sur GameMaker 1.4" dédié aux débutants.
https://www.youtube.com/playlist?list=PLI4mxhrGXQa-9aHFBg4hJfIkNpxqVzl2l

Vous pouvez utiliser les sources pour vos propres projets. Pour un usage des les supports visuels merci de citer le créateur.
Ce projet est lié directement à la communauté Francophone de GameMaker http://www.gmfrance.net